# Desafio HU
Basicamente o projeto utiliza duas rotas no NodeJS: "/" Que é a home do projeto e "/api/citties" Para consultar as cidades. Existe um documento no MongoDB chamado cities que tem algumas cidades de teste que são importadas do arquivo mongo-cities.js no deploy do projeto.

O projeto utiliza:
 - NodeJS
 - MongoDB
 - GruntJS
 - Jasmine
 - Loadtest

## Instalação
1 - Dê o Fork

2 - Clone
```sh
$ git clone git@bitbucket.org:a_souza/desafio-hu.git
``` 
3 - Entre no diretório do projeto
```sh
$ cd desafio-hu
``` 
4 - Rode o comando ./configure.sh para instalar as dependências do projeto
```sh
$ ./configure.sh
``` 

## Desenvolvimento do projeto
No arquivo Gruntfile.js estão todas as taks como: Compilar arquivo .sass, unificar e minificar os arquivos .js, executar testes de Lint e testes no jasmine. Para trabalhar no projeto rode o comando:
```sh
$ grunt watch
```
Se incluir for incluir um novo plugin js(/assets/js/vendor/plugin.js), rodar o comando:  
```sh
$ grunt watch
```
### Testes no JS
```sh
$ grunt uglify:plugins
```

### Testes de acesso
Para realizar os testes de acesso utilizei [loadtest]. Como 
```sh
$  loadtest -n 10000 -c 1000 http://localhost:4242/
```

[loadtest]: https://www.npmjs.com/package/loadtest
