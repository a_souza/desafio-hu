#Install MongoDB
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install -y mongodb-org

#Insert cities
mongo use hu
mongo hu mongo-cities.js

#Create a link to nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node

#Install NPM
sudo apt-get install npm

#Install Grunt
sudo npm install -g grunt-cli

#Install  modules
sudo npm install

#Install Loadtest
sudo npm install -g loadtest

#Running
nodejs app.js
