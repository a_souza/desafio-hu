module.exports = function (grunt) {

  var assets = {
    js_src: 'assets/js/',
    js_vendor: 'assets/js/vendor/',
    sass: 'sass/',
    css: 'assets/css/'
  };

  // Project configuration.
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    assets: assets,

    uglify: {
      plugins: {
        files: {
          '<%= assets.js_src %>plugins.min.js': [
            '<%= assets.js_vendor %>jquery-1.11.2.min.js',
            '<%= assets.js_vendor %>jquery-ui.min.js'
          ]
        }
      },
      global: {
        files: {
          '<%= assets.js_src %>global.min.js': [
            '<%= assets.js_src %>config.js',
            '<%= assets.js_src %>main.js'
          ]
        }
      },
    },
    compass: {
      dist: {
        options: {
          sassDir: 'sass',
          cssDir: '<%= assets.css %>',
          specify: '<%= assets.sass %>global.scss',
          outputStyle: 'compressed'
        }
      },
      dev: {
        options: {
          sassDir: 'sass',
          cssDir: '<%= assets.css %>',
          specify: '<%= assets.sass %>global.scss',
          outputStyle: 'nested'
        }
      }
    },
    watch: {
      js: {
        files: [
          '<%= assets.js_src %>**/*.js',
          '<%= assets.sass %>**/*.scss'],
        tasks: ['compass:dev', 'jshint:all', 'uglify:global'],
        options : { nospawn : true }
      }
    },
    jshint: {
      options: {
        ignores : [
          '<%= assets.js_src %>plugins.min.js',
          '<%= assets.js_src %>global.min.js'
        ]
      },
      all: [
        '<%= assets.js_src %>**.js',
      ]
    },
    jasmine: {
      test: {
        src: '<%= assets.js_src %>global.min.js',
        options: {
          vendor: [
            '<%= assets.js_src %>plugins.min.js',
            '<%= assets.js_vendor %>jasmine-jquery.js'
          ],
          specs: 'test/spec/*.spec.js'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-jasmine');

  grunt.registerTask('default', 'watch:js');
};