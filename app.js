var application_root = __dirname,
    express = require("express"),
    path = require("path"),
    mongoose = require('mongoose'),
    app = express();

// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(express.static(__dirname, 'assets'));


// Database
mongoose.connect('mongodb://localhost/hu');

var Schema, City, CityModel;

Schema = mongoose.Schema;
City = new Schema({
  name: { type: String, required: true },
  code: { type: String, required: true },
  modified: { type: Date, default: Date.now }
});
CityModel = mongoose.model('City', City);

app.get('/', function(req, res) {
  res.render('index');
});

app.get('/api/cities', function (req, res) {
  var regex = new RegExp(req.query["term"], 'i'),
      query =  CityModel.find({code: regex});

  query.exec(function (err,cities) {  
    if (!err) {
      var c = [];
      for(var object in cities){
        c.push(cities[object].name);
      }
      return res.json(c);
    } else {
      return console.log(err);
    }
  });
});

// Running
console.log("HU Challenge is running on http://localhost:4242");
app.listen(4242);