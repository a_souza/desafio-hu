jasmine.getFixtures().fixturesPath = 'test/fixtures';
jasmine.getStyleFixtures().fixturesPath = 'assets/css/';

describe("Testa os elementos de interface", function() {

  var labelPacotes, labelHoteis;

  beforeEach(function(){
    loadFixtures('side-search.html');
    loadStyleFixtures('global.css');
    loadStyleFixtures('vendor/jquery-ui.min.css');

    labelPacotes = $('#side-search .radios label:nth(0)');
    labelHoteis = $('#side-search .radios label:nth(1)');

  });

  it("Verifica o primeiro radio selecionado", function(){
    var check = labelHoteis.find('input').is(':checked');    
    expect(check).toBe(true);
  });

  it("Verifica o primeiro label está ativo", function(){
    expect(labelHoteis.hasClass('active')).toBe(true);
  });  

  it("Verifica se o click no label Pacotes recebe a class active", function(){    
    labelPacotes.trigger('click');     
    expect(labelPacotes.hasClass('active')).toBe(true);
  });

  it("Verifica se o click no label Pacotes check o radio", function(){    
    labelPacotes.trigger('click');     
    expect(labelPacotes.find('input').is(':checked')).toBe(true);
  });  

});