(function () {

  'use strict';

  var radios, labels;
  radios = $('.radios label input');
  labels = $('.radios label');

  function uncheck(){
    radios.attr('checked', '');
  }
  function removeCheck() {
    labels.removeClass('active');
  }

  $(function(){

    //Check radio trip
    var checkMark = (function () {
      $('body').on('click', '.radios label', function () {
        var el = $(this);
        removeCheck();
        uncheck();
        el.find('input').attr('checked', 'checked');
        el.addClass('active');
      });
    }());

    //Set Datepicker
    $(".check-date").datepicker({
      dateFormat: "dd/mm/yy",
      dayNamesMin: days,
      monthNames: months
    });

    // Set Autocomplte
    $("#side-search .city").autocomplete({
      source: '/api/cities',
      autoFocus: true,
    });

  });

}());